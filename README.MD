# cd-builder
[![pipeline status](https://gitlab.com/Antmounds/cd-builder/badges/master/pipeline.svg)](https://gitlab.com/Antmounds/cd-builder/commits/master) [![](https://images.microbadger.com/badges/image/antmounds/cd-builder.svg)](https://microbadger.com/images/antmounds/cd-builder "Optimized and ready to deploy") [![MIT license](http://img.shields.io/badge/license-MIT-blue.svg)](http://opensource.org/licenses/MIT) [![Discord Chat](https://img.shields.io/discord/299962468581638144.svg?logo=discord)](https://discord.gg/dw3Dam2)<br />![Dockerhub stats](http://dockeri.co/image/antmounds/cd-builder "Official Dockerhub image")

### Introduction
This image is used by Antmounds for continuous integration/deployment. Image is based on [Alpine Linux](https://alpinelinux.org/) and contains common devops tools like aws-cli, terraform and docker. This lightweight tool enables us to deploy multiple times per day and is open-source with the hope that it will deliver the same and more value to others.

### Tools
| Name           | Version |
|----------------|---------|
| aws-cli        | 1.15.28 |
| docker         | 18.06.0-ce, build 0ffa825 |
| docker-compose | 1.22.0-rc2, build 6817b533 |
| terraform      | 0.11.7 |


### Helpful commands
- Run the cd-builder image locally for testing adding your aws keys
	```
	$ docker run -it -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} antmounds/cd-builder sh
	```
- Run terraform on PWD
	```
	$ docker run -it -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} -v ${PWD}:home antmounds/cd-builder terraform init -upgrade=true
	```

## Contributing
Pull requests, forks and stars are 太好了 and mucho appreciatado!

- #### Get official Antmounds gear!
	<a href="https://shop.spreadshirt.com/Antmounds">
		<img src="https://image.spreadshirtmedia.com/content/asset/sprd-logo_horizontal.svg" width="160">
	</a>

- #### Become a Supporter!
	<a href="https://www.patreon.com/antmounds">
		<img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
	</a>

### MIT License
Copyright © 2018-present Antmounds.com, Inc. or its affiliates. All Rights Reserved.

>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

>View [license information](https://pkgs.alpinelinux.org/packages) for the software contained in this image.

>As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

>As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.